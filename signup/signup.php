<?php

use PEAR2\Net\RouterOS;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require_once '../vendor/autoload.php';
$alamat_login = 'http://10.1.1.1/login';
$def_password = substr(uniqid(), 8);
if (isset($_POST['email'])) {
    $useremail = strtolower($_POST['email']);
    $useremail .= "[Domain Name of email to use]";
    $client = new RouterOS\Client('[Router IP/Host}', '[Username Login]', '[Password]');
    $printRequest = new RouterOS\Request('/ip hotspot user print');
    $printRequest->setQuery(RouterOS\Query::where('name',$useremail));
    $activeUserEntry = $client->sendSync($printRequest);
    $username = $activeUserEntry->getProperty('name');
    
    if ($useremail == $username){
        echo "<script>alert('User $useremail already registered.');</script>";
    } else {
        $util = new RouterOS\Util($client);

        $util->setMenu('/ip hotspot user')->add(
            array(
                'name' => $useremail,
                'password' => $def_password,
                'profile' => 'default'
            )
        );
        $mail = new PHPMailer(true);
        try {
            $mail->isSMTP();                                            // Send using SMTP
            $mail->Host       = '';                    // Set the SMTP server to send through
            $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
            $mail->Username   = '';                     // SMTP username
            $mail->Password   = '';                               // SMTP password
            //$mail->SMTPAutoTLS = true;
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
            $mail->Port       = 465;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
            //$mail->SMTPOptions = array(
            //    'ssl' => array(
            //        'verify_peer' => false,
            //        'verify_peer_name' => false,
            //        'allow_self_signed' => true
            //    )
            //);
            //Recipients
            $mail->setFrom('[Email From]', '[Name]');
            $mail->addAddress($useremail);     // Add a recipient
    
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = 'Here is your credential for hotspot [...]';
            $mail->Body    = 'Your Username: ' . $useremail . ' Password: ' . $def_password . ' Please immediately change your password, when you are logged in.';
            $mail->AltBody = 'Your Username: ' . $useremail . ' Password: ' . $def_password . ' Please immediately change your password, when you are logged in.';
    
            $mail->send();
            // echo '<script>alert("Please check your email for credential!");</script>';
            echo "<script>var reload = alert('Check your email and get your credential, Click OK and return to login page.');

            if (reload != null) {
                window.location.href = '$alamat_login';
            }</script>";
        } catch (Exception $e) {
            echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        }
    }

}
?>
