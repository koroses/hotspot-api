# Hotspot API

So confuse what i've to name it. After all, here we go 'Hotspot API' written on PHP with [PHPMailer](https://github.com/PHPMailer/PHPMailer) 
for send the credential to user, and [Pear2/Net_RouterOS](https://github.com/pear2/Net_RouterOS) for communicate with 
MikroTik RouterOS API Protocol.

Back to the day that my office ran into connection issue that our Developer can't have stable and good internet connection via our wifi. 
After receive feedback from all of collegues, then i propose a solution with MikroTik for Bandwith management and Hotspot server for overcome.
Comes to my plan that we have a dozens employee here, it's time cosuming if we have to register one by one employee for their hotspot credential and so on.
Definitely we have to provide an apps so my collegues can get their credential by them self, and do a Change password account, Reset password if forgot.

Hopefully it can help you too if you have same circumstances on your environment

## Make it Work!

To have all work as expected you have to set the configuration precisely, below is the mandatory configuration that you have to take a look at.


1. Make sure you have a Server PC/Any Device that can host as PHP Server (MikroTik Can't do that), so make sure you have a Server that available
when it's needed.
2. The Device that host the PHP apps must connect to the router. You can make it via Wired Connection using LAN Cable or from Hotspot it self.
3. You must have a API User that can login to your MikroTik API interface from the host machine.
4. Make sure API service is allowed to access from the host PHP machine to the MikroTik Router.
5. Set a passthrough configuration on your firewall that make the router send a request with the real IP of client, so i change from masquerade rule 
for hotspot network to passthrough.
6. Add the ip of the host server (ip on the router network) to walled garden ip list so the un-authenticate user can access the apps from hotspot network 
(you can also add the email host of your email server, so the employee not have to switch to other connection for accessing their email).
7. If you use AP's for delivering the connection to client, dont forget to use a bridge network so your AP's not act masquerading the network.


That's all you have to concern about the configuration, if you have an issue regarding to your deployment you can contact me on [my linkedin](www.linkedin.com/in/fahri-noer). and Thank you!
