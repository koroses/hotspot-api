<?php

use PEAR2\Net\RouterOS;
error_reporting(0);
require_once '../vendor/autoload.php';
$errors = array();
try {
    //Adjust RouterOS IP, username and password accordingly.
    $client = new RouterOS\Client('[Router IP/Host}', '[Username Login]', '[Password]');
    $printRequest = new RouterOS\Request('/ip hotspot active print');
    $printRequest->setQuery(RouterOS\Query::where('address', $_SERVER['REMOTE_ADDR']));
    $activeUserEntry = $client->sendSync($printRequest);
    $user = $activeUserEntry->getProperty('user');
} catch (\Exception $e) {
    $errors[] = $e->getMessage();
	}
$util = new RouterOS\Util($client);
$old_pass = $util->setMenu('/ip hotspot user')->get(support, 'password');
echo $old_pass;