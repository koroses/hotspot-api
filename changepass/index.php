<?php
//tests
use PEAR2\Net\RouterOS;
error_reporting(0);
require_once '../vendor/autoload.php';
$errors = array();
try {
    //Adjust RouterOS IP, username and password accordingly.
    $client = new RouterOS\Client('[Router IP/Host}', '[Username Login]', '[Password]');
    $printRequest = new RouterOS\Request('/ip hotspot active print');
    $printRequest->setQuery(RouterOS\Query::where('address', $_SERVER['REMOTE_ADDR']));
    $activeUserEntry = $client->sendSync($printRequest);
    $user = $activeUserEntry->getProperty('user');
} catch (\Exception $e) {
    $errors[] = $e->getMessage();
}

if (isset($_POST['old_password']) && isset($_POST['password']) && isset($_POST['password2'])) {
    $util = new RouterOS\Util($client);
    $old_pass = $util->setMenu('/ip hotspot user')->get($user, 'password');
    if ($old_pass !== $_POST['old_password']) {
        $errors[] = 'Wrong password.';
    } else if ($_POST['password'] !== $_POST['password2']) {
        $errors[] = 'Passwords do not match.';
    } else {
        //Here's the fun part - actually changing the password
        $setRequest = new RouterOS\Request('/ip hotspot user set');
        $client($setRequest
            ->setArgument('password', $_POST['password'])
            ->setArgument('numbers', $client($printRequest
                ->setCommand('/ip hotspot user print')
                ->setArgument('.proplist', '.id')
                ->setQuery(RouterOS\Query::where('name', $activeUserEntry->getProperty('user'))))->getProperty('.id')));
    echo '<script>alert("Your password has been changed successfully!");</script>';
    }
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Change Hotspot Password</title>
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="expires" content="-1" />	
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0" />
<link rel="icon" href="logo.png"/>
<link rel="stylesheet" href="login.css" media="screen">
</head>
<body class='login'>
<form class="vertical-form" name="login" action="" method="post" background="#A03472">
<div style="margin:0;padding:50;display:inline"></div>

<center>
    <div id="head">
    </div>
    
    
<div id="box">
<div id="userdiv"> </div>
<br>
<?php if ($user == '') { ?>
            <legend>Please login to hotspot first</legend>
        <?php } else if (!isset($activeUserEntry)) { ?>
            <legend>We're sorry, but we can't change your password right now. Please try again later</legend>
        <?php } else { ?>
            <legend>You're currently logged in as "<?php
                                                //echo $activeUserEntry->getArgument('user');
                                                echo $activeUserEntry->getProperty('user');
                                                ?>"</legend>

            <?php if (!empty($errors)) { ?>
                <div id="errors">
                    <ul>
                        <?php foreach ($errors as $error) { ?>
                            <li><?php echo $error; ?></li>
                        <?php } ?>
                    </ul>
                </div>
             <?php } ?>
    <!-- <div id="userdiv"> </div>
    <legend>
    <p>Reset Password</p>
    </legend> -->
<div><input id="pass" autocomplete="off" name="old_password" type="password" placeholder="Old password"/></div>
<div><input id="pass" autocomplete="off" name="password" type="password" placeholder="New password"/></div>
<div><input id="pass" autocomplete="off" name="password2" type="password" placeholder="Confirm new password"/></div>
<br>
<div><input id="button" name="submit" type="submit" value="Change" /></div>
</div>
</center>
<?php } ?>
</form>
</body>
</html>